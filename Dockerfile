FROM java:8

COPY /target/Schioppa-1.0-SNAPSHOT.jar /opt/apps/schioppa.jar
COPY /src/main/resources/application.yml /usr/local/keys/application.yml

EXPOSE 8080

RUN chmod -R -776 /usr/local/keys/
RUN chmod -R -776 /opt/apps/schioppa.jar

ENTRYPOINT ["java", "-Xmx512m", "-Xms256m", "-jar", "/opt/apps/schioppa.jar”, ”—spring.config.location=/​usr/​local/keys/"]
