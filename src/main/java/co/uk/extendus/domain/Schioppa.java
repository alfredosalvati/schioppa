package co.uk.extendus.domain;

import java.time.LocalDate;

/**
 * Created by asalvati on 14/11/2016.
 */
public class Schioppa {

    public enum Type {
        BLACK, WHITE
    }

    private String name;
    private Type type;
    private Integer year;
    private String creatorName;


    public Schioppa(final String name, final Type type, final Integer date, final String creatorName) {
        this.creatorName = creatorName;
        this.type = type;
        this.name = name;
        this.year = date;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public Integer getYear() {
        return year;
    }

    public String getCreatorName() {
        return creatorName;
    }
}
