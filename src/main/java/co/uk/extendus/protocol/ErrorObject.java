package co.uk.extendus.protocol;

/**
 * Created by asalvati on 18/11/2016.
 */
public class ErrorObject {

    String errorCode;
    String errorMessage;

    public ErrorObject(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
