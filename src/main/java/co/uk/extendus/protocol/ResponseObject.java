package co.uk.extendus.protocol;

import java.util.Map;

/**
 * Created by asalvati on 14/11/2016.
 */
public class ResponseObject {

    private String id;

    private Map<String, Object> result;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }


}
