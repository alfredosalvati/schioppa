package co.uk.extendus.protocol;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by asalvati on 14/11/2016.
 */

public class RequestObject<T>  {

    @NotEmpty
    @Size(max=128)
    private String method;
    @NotEmpty
    private String id;

    @Valid
    @NotNull
    private T parameters;

    public RequestObject(){

    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public T getParameters() {
        return parameters;
    }

    public void setParameters(T parameters) {
        this.parameters = parameters;
    }
}
