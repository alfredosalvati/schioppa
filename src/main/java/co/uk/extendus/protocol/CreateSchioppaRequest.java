package co.uk.extendus.protocol;


import co.uk.extendus.validator.ValidateType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by asalvati on 14/11/2016.
 */

public class CreateSchioppaRequest {

    @NotEmpty
    @NotNull
    private String name;

    @NotEmpty
    @ValidateType( acceptedValues={"Black", "White"}, message = "Not a valid Schioppa Type")
    private String type;


    @Min(10)
    private Integer year;

    @NotEmpty
    private String creatorName;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
