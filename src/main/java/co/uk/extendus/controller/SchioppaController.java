package co.uk.extendus.controller;

import co.uk.extendus.exception.SchioppaException;
import co.uk.extendus.protocol.CreateSchioppaRequest;
import co.uk.extendus.protocol.RequestObject;
import co.uk.extendus.service.SchioppaService;
import co.uk.extendus.validator.ConstraintValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import co.uk.extendus.domain.Schioppa;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asalvati on 14/11/2016.
 */

@RestController
@RequestMapping("schioppa")
public class SchioppaController  {

    @Autowired
    private ConstraintValidator constraintValidator;

    @Autowired
    private SchioppaService schioppaService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(constraintValidator);
    }

    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createSchioppa(final @Valid @RequestBody RequestObject<CreateSchioppaRequest> request) throws Exception {

        Schioppa schioppa = new Schioppa(request.getParameters().getName(), Schioppa.Type.valueOf(request.getParameters().getType().toUpperCase()),
                request.getParameters().getYear(), request.getParameters().getCreatorName());
        if(schioppaService.createSchioppa(schioppa)) {

            return ResponseEntity.status(HttpStatus.OK).body(request.getParameters());
        }
        else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @ResponseBody
    @RequestMapping(value ="/findByCreatorName", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findByCreatorName(@RequestParam String creatorName) throws Exception {
        try {
            List<Schioppa> list = schioppaService.findByCreatorName(creatorName);
            if (list.size() > 0) {
                return ResponseEntity.status(HttpStatus.OK).body(list);
            } else return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(value ="/findByYear", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findByYear(@RequestParam Integer year) throws Exception {
        try {
            List<Schioppa> list = schioppaService.findByYear(year);
            if (list.size() > 0) {
                return ResponseEntity.status(HttpStatus.OK).body(list);
            } else return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @ResponseBody
    @RequestMapping(value ="/findByName", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findByName(@RequestParam String name) throws Exception {
        try {
            List<Schioppa> list = schioppaService.findByName(name);
            if (list.size() > 0) {
                return ResponseEntity.status(HttpStatus.OK).body(list);
            } else return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    @ResponseBody
    @RequestMapping(value ="/delete", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@RequestParam String name) throws Exception {
        try {
            if (schioppaService.deleteSchioppa(name)) {
                return ResponseEntity.status(HttpStatus.OK).body(null);
            } else return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
