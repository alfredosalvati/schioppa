package co.uk.extendus.controller;

import co.uk.extendus.exception.SchioppaException;
import co.uk.extendus.protocol.ErrorObject;
import co.uk.extendus.protocol.ResponseObject;
import org.codehaus.jackson.annotate.JsonRawValue;
import org.codehaus.jackson.annotate.JsonValue;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by asalvati on 17/11/2016.
 */
@ControllerAdvice
public class ExceptionController {



    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseEntity exception(Exception e, HttpServletRequest httpRequest) {

        ResponseEntity response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        return response;

    }


    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity validationException(MethodArgumentNotValidException e, HttpServletRequest httpRequest) {

        ResponseEntity response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(populateErrorMesage(e));
        return response;

    }

    private  List<ErrorObject>  populateErrorMesage(MethodArgumentNotValidException e) {
        HashMap<String, Object> errorMap = new HashMap<>();
        List<ErrorObject> errorMessage = new ArrayList<>();
        e.getBindingResult().getFieldErrors().forEach( fieldError ->
                errorMessage.add(new ErrorObject(fieldError.getField(), fieldError.getCode())));
        errorMap.put("errorMap", errorMessage);
        return errorMessage;
    }
}

