package co.uk.extendus.service;

import co.uk.extendus.domain.Schioppa;

import java.util.List;

/**
 * Created by asalvati on 21/11/2016.
 */
public interface SchioppaService {
    public boolean createSchioppa(Schioppa schioppa);
    public List<Schioppa> findByName(String name);
    public List<Schioppa> findByCreatorName(String name);
    public List<Schioppa> findByYear(Integer year);
    public boolean deleteSchioppa(String name);

}
