package co.uk.extendus.service;

import co.uk.extendus.domain.Schioppa;
import com.sun.javafx.collections.SetListenerHelper;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by asalvati on 21/11/2016.
 */

@Service
public class SchioppaServiceImpl implements SchioppaService {


    Set<Schioppa> schioppaList = new HashSet<>();

    @Override
    public boolean createSchioppa(Schioppa schioppa) {
        if(schioppaList.stream().anyMatch(s -> s.getName().equalsIgnoreCase(schioppa.getName()))) {
            return false;
        }
        schioppaList.add(schioppa);
        return true;
    }

    @Override
    public List<Schioppa> findByName(String name) {
        return schioppaList.stream().filter(schioppa -> schioppa.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }

    @Override
    public List<Schioppa> findByCreatorName(String name) {
        return schioppaList.stream().filter(schioppa -> schioppa.getCreatorName().equalsIgnoreCase(name)).collect(Collectors.toList());
    }

    @Override
    public List<Schioppa> findByYear(Integer year) {
        return schioppaList.stream().filter(schioppa -> schioppa.getYear()==year).collect(Collectors.toList());

    }

    @Override
    public boolean deleteSchioppa(String name) {
        return schioppaList.removeIf(schioppa -> schioppa.getName().equalsIgnoreCase(name));

    }
}
