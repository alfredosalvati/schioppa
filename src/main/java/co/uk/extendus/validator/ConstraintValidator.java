package co.uk.extendus.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

@Component
public class ConstraintValidator implements Validator{

	@Autowired
	private HttpServletRequest request;
	
	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	@Override
	public void validate(Object object, Errors arg1) {
		//Validate the constrains annotation in the Object
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		javax.validation.Validator validator = validatorFactory.getValidator();
		Set<ConstraintViolation< Object >> violations = validator.validate(object);
		for (ConstraintViolation<Object> v : violations) {
			arg1.rejectValue(v.getPropertyPath().toString(), v.getMessage());
		}

	}
	

}
