package co.uk.extendus.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asalvati on 17/11/2016.
 */
public class TypeValidator implements ConstraintValidator<ValidateType, String> {

    private List<String> valueList;

    @Override
    public void initialize(ValidateType constraintAnnotation) {
        valueList = new ArrayList<>();
        for(String val : constraintAnnotation.acceptedValues()) {
            valueList.add((val.toUpperCase()));
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(!valueList.contains(value.toUpperCase())) {
            return false;
        }
        return true;
    }
}
