package co.uk.extendus.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by asalvati on 17/11/2016.
 */

@Target(ElementType.FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = TypeValidator.class)
@Documented
public @interface ValidateType {

    String[] acceptedValues();

    String message() default "{uk.co.extendus.validator.invalidType}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
