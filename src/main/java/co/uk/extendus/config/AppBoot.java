package co.uk.extendus.config;


import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by asalvati on 14/11/2016.
 */

@SpringBootApplication(scanBasePackages = "co.uk.extendus")
public class AppBoot {
    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(AppBoot.class);
        app.run(args);
	System.out.println(“AA”);
    }
}
