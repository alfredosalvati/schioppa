package uk.co.extendus.test.integration;

import co.uk.extendus.config.AppBoot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by asalvati on 21/11/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppBoot.class)
@WebAppConfiguration
public class SchioppaIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void testCreateSchioppa() throws Exception {
        mvc.perform(post("/schioppa/create")
            .content("{\"method\":\"create\",\t\"id\":\"123\",\"parameters\" : {\"name\":\"Gisele\",\"year\" : \"2001\",\"type\" : \"white\",\"creatorName\" : \"alfredo\"}}")
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(content().json("{\"name\":\"Gisele\",\"type\":\"white\",\"year\":2001,\"creatorName\":\"alfredo\"}"));
    }

    @Test
    public void testFindByCreatorName() throws Exception {
        mvc.perform(post("/schioppa/create")
                .content("{\"method\":\"create\",\t\"id\":\"123\",\"parameters\" : {\"name\":\"Gisele\",\"year\" : \"2001\",\"type\" : \"white\",\"creatorName\" : \"alfredo\"}}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("{\"name\":\"Gisele\",\"type\":\"white\",\"year\":2001,\"creatorName\":\"alfredo\"}"));


        mvc.perform(get("/schioppa/findByCreatorName?creatorName=alfredo"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("[{\"name\":\"Gisele\",\"type\":\"WHITE\",\"year\":2001,\"creatorName\":\"alfredo\"}]"));

    }

    @Test
    public void testCreateTwice() throws Exception {
        mvc.perform(post("/schioppa/create")
                .content("{\"method\":\"create\",\t\"id\":\"123\",\"parameters\" : {\"name\":\"Gisele\",\"year\" : \"2001\",\"type\" : \"white\",\"creatorName\" : \"alfredo\"}}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("{\"name\":\"Gisele\",\"type\":\"white\",\"year\":2001,\"creatorName\":\"alfredo\"}"));

        mvc.perform(post("/schioppa/create")
                .content("{\"method\":\"create\",\t\"id\":\"123\",\"parameters\" : {\"name\":\"Gisele\",\"year\" : \"2001\",\"type\" : \"white\",\"creatorName\" : \"alfredo\"}}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isConflict());


    }


    @Test
    public void testType() throws Exception {
        mvc.perform(post("/schioppa/create")
                .content("{\"method\":\"create\",\t\"id\":\"123\",\"parameters\" : {\"name\":\"Gisele\",\"year\" : \"2001\",\"type\" : \"blue\",\"creatorName\" : \"alfredo\"}}")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("[{\"errorCode\":\"parameters.type\",\"errorMessage\":\"Not a valid Schioppa Type\"}]"));


    }


}
