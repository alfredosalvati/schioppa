package uk.co.extendus.test.unit;

import co.uk.extendus.domain.Schioppa;
import co.uk.extendus.service.SchioppaService;
import co.uk.extendus.service.SchioppaServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by asalvati on 21/11/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class SchioppaUnitTest {


    private SchioppaServiceImpl schioppaService = new SchioppaServiceImpl();

    @Test
    public void tetsSchioppaCreationWhite() {
        Schioppa schioppa = new Schioppa("Jessica", Schioppa.Type.valueOf("WHITE"), 2015, "Alfredo");

        Assert.assertEquals(schioppa.getName(), "Jessica");

        Assert.assertEquals(schioppa.getType().toString(), "WHITE");
        Assert.assertEquals(schioppa.getYear().intValue(), 2015);
        Assert.assertEquals(schioppa.getCreatorName(), "Alfredo");
        Assert.assertEquals(schioppa.getName(), "Jessica");
    }

    @Test
    public void tetsSchioppaCreationBlack() {
        Schioppa schioppa = new Schioppa("Jessica", Schioppa.Type.valueOf("BLACK"), 2015, "Alfredo");

        Assert.assertEquals(schioppa.getName(), "Jessica");

        Assert.assertEquals(schioppa.getType().toString(), "BLACK");
        Assert.assertEquals(schioppa.getYear().intValue(), 2015);
        Assert.assertEquals(schioppa.getCreatorName(), "Alfredo");
        Assert.assertEquals(schioppa.getName(), "Jessica");
    }

    @Test(expected =  IllegalArgumentException.class)
    public void tetsSchioppaCreationBadType() {
        Schioppa schioppa = new Schioppa("Jessica", Schioppa.Type.valueOf("white"), 2015, "Alfredo");

    }

    @Test
    public void testAddSchioppaAndFind() {
        Schioppa schioppa = new Schioppa("Jessica", Schioppa.Type.valueOf("BLACK"), 2015, "Alfredo");

        schioppaService.createSchioppa(schioppa);
        Assert.assertFalse(schioppaService.findByCreatorName("Alfredo").isEmpty());
        Assert.assertTrue(schioppaService.findByCreatorName("Alfredo").size() == 1);
        Assert.assertTrue(schioppaService.findByCreatorName("Alfredo").get(0).getName().equalsIgnoreCase("jessica"));

    }
}
